FROM node:16.0.0-alpine

#-------------------------------------------------------------------------------
# User

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

RUN set -exo pipefail                                && \
                                                        \
    echo 'Creating the git user and group from host' && \
    deluser --remove-home node                       && \
    addgroup -g $HOST_USER_GID -S node               && \
    adduser -u $HOST_USER_UID -G node -D node        && \
                                                        \
    echo 'Set direcotry permissions'                 && \
    mkdir /work                                      && \
    chown node:node /work
