#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

# By default the `help` goal is executed.
.DEFAULT_GLOBAL := help

HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v $(HERE):/here -w /here

#-------------------------------------------------------------------------------
# Utility goals
#-------------------------------------------------------------------------------

# # help
#
# Displays a help message containing usage instructions and a list of available
# goals.
#
# Usage:
#
# To display the help message run:
# ```bash
# make help
# ```
#
# This is also the default goal so you can run it as following:
# ```bash
# make
# ```
.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo '  make css/webfonts'
	@echo ''
	@echo 'Utility goals:'
	@echo '  - help: Displays this help message.'
	@echo '  - open: Open the main webpage in a browser.'

# # open
#
# Open the main webpage in a browser.
#
# Usage:
#
# ```bash
# make open
# ```
.PHONY: open
open:
	@echo 'Opening: http://localhost:1313'
	@xdg-open http://localhost:1313

.PHONY: build
build:
	@docker build \
		--build-arg=HOST_USER_UID=`id -u` \
		--build-arg=HOST_USER_GID=`id -g` \
		-t true-paper-css .

.PHONY: shell
shell:
	@envchain npm $(DOCKER_HERE) -e NPM_TOKEN true-paper-css sh

.PHONY: sass
sass:
	@$(DOCKER_HERE) sdwolfz/sass:latest sass \
		src/true-paper.scss:src/true-paper.css
	@$(DOCKER_HERE) sdwolfz/sass:latest sass \
		--style=compressed                     \
		--no-source-map                        \
		src/true-paper.scss:src/true-paper.min.css
	@gzip -9 -f -k src/true-paper.min.css

.PHONY: generate
generate:
	@ruby generate.rb
