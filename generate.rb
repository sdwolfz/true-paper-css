#!/usr/bin/env ruby

require 'erb'

TEMPLATE = File.read('test-template.erb')
WORKLOAD = [
  {
    size:        'A3',
    orientation: 'portrait',
    dimensions:  'A3 portrait'
  },
  {
    size:        'A3',
    orientation: 'landscape',
    dimensions:  'A3 landscape'
  },
  {
    size:        'A4',
    orientation: 'portrait',
    dimensions:  'A4 portrait'
  },
  {
    size:        'A4',
    orientation: 'landscape',
    dimensions:  'A4 landscape'
  },
  {
    size:        'A5',
    orientation: 'portrait',
    dimensions:  'A5 portrait'
  },
  {
    size:        'A5',
    orientation: 'landscape',
    dimensions:  'A5 landscape'
  },
  {
    size:        'card-european',
    orientation: 'portrait',
    dimensions:  '55mm 85mm'
  },
  {
    size:        'card-european',
    orientation: 'landscape',
    dimensions:  '85mm 55mm'
  },
  {
    size:        'legal',
    orientation: 'portrait',
    dimensions:  'legal portrait'
  },
  {
    size:        'legal',
    orientation: 'landscape',
    dimensions:  'legal landscape'
  },
  {
    size:        'letter',
    orientation: 'portrait',
    dimensions:  'letter portrait'
  },
  {
    size:        'letter',
    orientation: 'landscape',
    dimensions:  'letter landscape'
  }
].freeze

renderer = ERB.new(TEMPLATE, trim_mode: '-')

WORKLOAD.each do |payload|
  @size        = payload[:size]
  @orientation = payload[:orientation]
  @dimensions  = payload[:dimensions]

  ['strict', 'lax'].each do |rigor|
    @count  = 1
    @style  = 'Single'
    @rigor  = rigor
    path    = "test/#{@size}-#{@orientation}-single-#{@rigor}.html"
    content = renderer.result(binding)
    File.write(path, content)

    @count  = 3
    @style  = 'Multi'
    @rigor  = rigor
    path    = "test/#{@size}-#{@orientation}-multi-#{@rigor}.html"
    content = renderer.result(binding)
    File.write(path, content)

    @count  = 1000
    @style  = 'Thousand'
    @rigor  = rigor
    path    = "test/#{@size}-#{@orientation}-thousand-#{@rigor}.html"
    content = renderer.result(binding)
    File.write(path, content)
  end
end
